package pilas.operaciones;

import java.util.Scanner;
import java.util.Stack;


public class ACTIVIDAD3_PILAS_OPERACIONES {

     public static void main (String [] args) {
        Scanner entrada = new Scanner(System.in);
        String respuesta = "S";
        String cadena="";
        String numeroA="";
        String numeroE="";
        String numeroI="";
        String numeroO="";
        String numeroU="";

        while (respuesta.equalsIgnoreCase("S")){
            System.out.println ("escriba una cadena de texto");
            cadena=entrada.nextLine();
            switch (analizarA(cadena)){
                case 1: numeroA="par"; break;
                case -1: numeroA="impar"; break;
                case 0: numeroA= "cero"; break;
            }
            switch (analizarE(cadena)){
                case 1: numeroE="par"; break;
                case -1: numeroE="impar"; break;
                case 0: numeroE= "cero"; break;
            }
            switch (analizarI(cadena)){
                case 1: numeroI="par"; break;
                case -1: numeroI="impar"; break;
                case 0: numeroI= "cero"; break;
            }
            switch (analizarO(cadena)){
                case 1: numeroO="par"; break;
                case -1: numeroO="impar"; break;
                case 0: numeroO= "cero"; break;
            }
            switch (analizarU(cadena)){
                case 1: numeroU="par"; break;
                case -1: numeroU="impar"; break;
                case 0: numeroU= "cero"; break;
            }
            System.out.println ("El número de letras a es "+numeroA+",/ "
                    + "el número de letras e es "+numeroE+",/"
                    + " el número de letras i es "+numeroI+",/"
                    + " el número de letras o es "+numeroO+" /"
                    + "y el número de letras u es "+numeroU+".");
            System.out.print ("¿Desea analizar otra cadena? (S/N) ");
            respuesta = entrada.nextLine();
        }
    }

    public static int analizarA (String cadena) {
        Stack<String> pila = new Stack<String>(); int i=0; int auxiliar=0;
        while (i<cadena.length()) {
            if (cadena.charAt(i)=='a'&&pila.empty()) {pila.push("a"); auxiliar++;}
            else if (cadena.charAt(i)=='a'&&!pila.empty()) {pila.pop();}
            i++;
        }
        if (auxiliar==0) {return 0;} else { if (pila.empty()) {return 1;} else {return -1;}}     
    }

    public static int analizarE (String cadena) {
        Stack<String> pila = new Stack<String>(); int i=0; int auxiliar=0;
        while (i<cadena.length()) {
            if (cadena.charAt(i)=='e'&&pila.empty()) {pila.push("e"); auxiliar++;}
            else if (cadena.charAt(i)=='e'&&!pila.empty()) {pila.pop();}
            i++;
        }
        if (auxiliar==0) {return 0;} else { if (pila.empty()) {return 1;} else {return -1;}}
    }

    public static int analizarI (String cadena) {
        Stack<String> pila = new Stack<String>(); int i=0; int auxiliar=0;
        while (i<cadena.length()) {
            if (cadena.charAt(i)=='i'&&pila.empty()) {pila.push("i"); auxiliar++;}
            else if (cadena.charAt(i)=='i'&&!pila.empty()) {pila.pop();}
            i++;
        }       
        if (auxiliar==0) {return 0;} else { if (pila.empty()) {return 1;} else {return -1;}}
    }

    public static int analizarO (String cadena) {
        Stack<String> pila = new Stack<String>(); int i=0; int auxiliar=0;
        while (i<cadena.length()) {
            if (cadena.charAt(i)=='o'&&pila.empty()) {pila.push("o"); auxiliar++;}
            else if (cadena.charAt(i)=='o'&&!pila.empty()) {pila.pop();}
            i++;
        }
        if (auxiliar==0) {return 0;} else { if (pila.empty()) {return 1;} else {return -1;}}     
    }

    public static int analizarU (String cadena) {
        Stack<String> pila = new Stack<String>(); int i=0; int auxiliar=0;
        while (i<cadena.length()) {
            if (cadena.charAt(i)=='u'&&pila.empty()) {pila.push("u"); auxiliar++;}
            else if (cadena.charAt(i)=='u'&&!pila.empty()) {pila.pop();}
            i++;
        }
        if (auxiliar==0) {return 0;} else { if (pila.empty()) {return 1;} else {return -1;}}
        
    }
    }

